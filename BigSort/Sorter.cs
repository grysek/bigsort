﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigSort
{
    class Sorter
    {
        private CultureInfo CultureInfo { get; set; } = new CultureInfo("en-US");

        public int CoreNumber { get; set; } = 2;
        public int ChunkSizeInBytes { get; set; } = 50000000;
        public int NumberOfElementsInChunk => ChunkSizeInBytes / 4; //float is a 32-bit type so one number takes 4 bytes

        private CancellationToken token;
        private List<Task> tasks;
        private ChunkLibrary library;

        public async Task<IEnumerable<string>> Sort(IEnumerable<string> input, CancellationToken token, ChunkLibrary library)
        {
            this.token = token;
            this.library = library;

            var enumerator = input.GetEnumerator();
            tasks = new List<Task>();

            await DivideAndSortChunks(enumerator);
            await MergeChunks(token);

            var v = library.GetFirstName();
            
            return FileUtils.GetBinary(v, token).Select(f => f.ToString(CultureInfo));
        }

        private async Task MergeChunks(CancellationToken token)
        {
            int mergesLeft = library.Count - 1;

            while (mergesLeft > 0)
            {
                while (library.Count < 2)
                    await Task.WhenAny(tasks);

                var first = library.PopFirstName();
                var second = library.PopFirstName();
                mergesLeft--;

                await RunTask(tasks, () => library.Store(Merge(FileUtils.GetBinary(first, token), FileUtils.GetBinary(second, token))),
                    t =>
                    {
                        File.Delete(first);
                        File.Delete(second);
                    });
            }

            await Task.WhenAll(tasks);
        }

        private async Task DivideAndSortChunks(IEnumerator<string> enumerator)
        {
            while (true)
            {
                var chunk = GetChunk(enumerator);
                if (token.IsCancellationRequested || chunk.Count == 0) break;

                await RunTask(tasks, () => SortChunk(chunk), t => library.Store(chunk));
            }

            await Task.WhenAll(tasks);
        }

        private async Task RunTask(List<Task> tasks, Action task, Action<Task> onFinish)
        {
            if (tasks.Count == CoreNumber)
            {
                await Task.WhenAny(tasks);
                RefreshTaskList(tasks);
            }

            tasks.Add(Task.Factory.StartNew(task).ContinueWith(onFinish));
        }

        private IEnumerable<float> Merge(IEnumerable<float> first, IEnumerable<float> second)
        {
            var e1 = first.GetEnumerator();
            var e2 = second.GetEnumerator();

            bool hasNext1 = e1.MoveNext();
            bool hasNext2 = e2.MoveNext();

            while (true)
            {
                if (!hasNext1 && !hasNext2)
                    break;
                else if (!hasNext1)
                {
                    var f2 = e2.Current;
                    hasNext2 = e2.MoveNext();
                    yield return f2;
                }
                else if (!hasNext2)
                {
                    var f1 = e1.Current;
                    hasNext1 = e1.MoveNext();
                    yield return f1;
                }
                else
                {
                    var f1 = e1.Current;
                    var f2 = e2.Current;

                    if (f1 < f2)
                    {
                        hasNext1 = e1.MoveNext();
                        yield return f1;
                    }
                    else
                    {
                        hasNext2 = e2.MoveNext();
                        yield return f2;
                    }
                }
            }
        }

        private void RefreshTaskList(List<Task> tasks)
        {
            var completed = tasks.Where(t => t.IsCompleted).ToList();
            completed.ForEach(t => tasks.Remove(t));
        }

        private void SortChunk(List<float> chunk) => chunk.Sort();
      
        private List<float> GetChunk(IEnumerator<string> enumerator)
        {
            int count = NumberOfElementsInChunk;
            var items = new List<float>();

            while (enumerator.MoveNext() && (count-- > 0))
            {
                items.Add(Parse(enumerator.Current));
            }

            return items;
        }

        private float Parse(string number)
        {
            return float.Parse(number, CultureInfo);
        }
    }
}
