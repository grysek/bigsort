﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigSort
{
    class FileUtils
    {
        public static IEnumerable<string> GetLines(string path, CancellationToken token)
        {
            using (var fstream = new FileStream(path, FileMode.Open))
            {
                fstream.Seek(0, SeekOrigin.Begin);
                var reader = new StreamReader(fstream);

                string line;
                while (!token.IsCancellationRequested && ((line = reader.ReadLine()) != null))
                    yield return line;
            }
        }

        public static void SaveLines(string path, IEnumerable<string> lines)
        {
            using (var fstream = new FileStream(path, FileMode.Create))
            {
                fstream.Seek(0, SeekOrigin.Begin);
                var writer = new StreamWriter(fstream);

                foreach (var line in lines)
                    writer.WriteLine(line);
            }
        }

        public static void SaveBinary(string path, IEnumerable<float> lines)
        {
            using (var fstream = new FileStream(path, FileMode.Create))
            {
                fstream.Seek(0, SeekOrigin.Begin);
                var writer = new BinaryWriter(fstream);

                foreach (var line in lines)
                    writer.Write(line);
            }
        }

        public static IEnumerable<float> GetBinary(string path, CancellationToken token)
        {
            using (var fstream = new FileStream(path, FileMode.Open))
            {
                fstream.Seek(0, SeekOrigin.Begin);
                var reader = new BinaryReader(fstream);

                while (!token.IsCancellationRequested)
                {
                    float item;
                    try
                    {
                        item = reader.ReadSingle();
                    }
                    catch (EndOfStreamException)
                    {
                        break;
                    }
                    yield return item;
                }
            }
        }
    }
}
