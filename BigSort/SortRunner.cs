﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BigSort
{
    class SortRunner
    {
        private CancellationTokenSource cancellation;

        public Sorter Sorter { get; private set; }

        public SortRunner()
        {
            Sorter = new Sorter();
        }

        public async Task RunOnFile(string path)
        {
            await Task.Run(async () => await Job(path));
        }

        private async Task Job(string path)
        {
            var fileDir = Path.GetDirectoryName(path);

            using (var library = new ChunkLibrary(Path.Combine(fileDir, "tmp")))
            {
                cancellation = new CancellationTokenSource();
                var token = cancellation.Token;
                var lines = FileUtils.GetLines(path, token);
                var result = await Sorter.Sort(lines, token, library);
                if (!token.IsCancellationRequested)
                    FileUtils.SaveLines(path, result);
            }
        }

        public void Stop()
        {
            cancellation.Cancel();
        }
    }
}
