﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigSort
{
    class OpenFileDialogFilePicker : IFilePickerHelper
    {
        public string Path { get; private set; }

        public bool PickFile(string filter)
        {
            var dialog = new OpenFileDialog() { Filter = filter};
            var result = dialog.ShowDialog();

            Path = result == true ? dialog.FileName : null;

            return (bool)result;
        }
    }
}
