﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigSort
{
    public interface IFilePickerHelper
    {
        string Path { get; }
        bool PickFile(string filter);
    }
}
