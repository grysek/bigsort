﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigSort
{
    class ChunkLibrary : IDisposable
    {
        private List<string> list;
        private string tmpDir;

        public ChunkLibrary(string tmpDir)
        {
            this.tmpDir = tmpDir;
            if (!Directory.Exists(tmpDir))
                Directory.CreateDirectory(tmpDir);
            DeleteTmpFiles();
            list = new List<string>();
        }

        public int Count => list.Count;

        public string GetFirstName()
        {
            return list.First();
        }

        public string PopFirstName()
        {
            var first = list.First();
            list.Remove(first);
            return first;
        }

        internal void Clear()
        {
            list.Clear();
            DeleteTmpFiles();
        }

        public void Store(IEnumerable<float> chunk)
        {
            var name = Path.Combine(tmpDir, $"{Path.GetRandomFileName()}.tmp");
            FileUtils.SaveBinary(name, chunk);
            list.Add(name);
        }

        private void DeleteTmpFiles()
        {
            var dir = new DirectoryInfo(tmpDir);
            foreach (var file in dir.GetFiles())
                file.Delete();
        }

        public void Dispose()
        {
            Directory.Delete(tmpDir, true);
        }
    }
}
