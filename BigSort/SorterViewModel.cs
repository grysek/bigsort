﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace BigSort
{
    public class SorterViewModel : BindableBase
    {
        public DelegateCommand RunCommand { get; private set; }
        public DelegateCommand CancelCommand { get; private set; }
        public DelegateCommand SelectFileCommand { get; private set; }

        public ObservableCollection<int> CoreNumberValues { get; private set; }
        public ObservableCollection<float> ChunkSizeValues { get; private set; }

        public string FilePath
        {
            get { return this.filePath; }
            private set
            {
                SetProperty(ref this.filePath, value);
                RunCommand.RaiseCanExecuteChanged();
            }
        }

        public float ChunkSize
        {
            get { return this.chunkSize; }
            private set { SetProperty(ref this.chunkSize, value); }
        }

        public int CoreNumber
        {
            get { return this.coreNumber; }
            private set { SetProperty(ref this.coreNumber, value); }
        }

        public string Status
        {
            get { return this.status; }
            private set { SetProperty(ref this.status, value); }
        }

        public string Time
        {
            get { return this.time; }
            private set { SetProperty(ref this.time, value); }
        }

        public bool IsBusy
        {
            get { return this.isBusy; }
            private set
            {
                SetProperty(ref this.isBusy, value);
                Status = value ? "Busy" : "Ready";
                RunCommand.RaiseCanExecuteChanged();
                CancelCommand.RaiseCanExecuteChanged();
                SelectFileCommand.RaiseCanExecuteChanged();
            }
        }

        private bool isBusy;
        private string status;
        private string time;
        private int coreNumber;
        private SortRunner sortRunner;
        private float chunkSize;
        private IFilePickerHelper filePicker;
        private string filePath;
        private Stopwatch stopwatch;
        private DispatcherTimer timer;

        public SorterViewModel(IFilePickerHelper filePicker)
        {
            this.filePicker = filePicker;

            RunCommand = new DelegateCommand(Run, CanRun);
            CancelCommand = new DelegateCommand(Cancel, CanCancel);
            SelectFileCommand = new DelegateCommand(SelectFile, CanSelectFile);

            CoreNumberValues = new ObservableCollection<int> { 1, 2, 4, 8, 16 };
            ChunkSizeValues = new ObservableCollection<float> { 0.5F, 1F, 10F, 100F, 500F, 1000F };

            CoreNumber = 16;
            ChunkSize = 100F;

            sortRunner = new SortRunner();
        }

        private bool CanSelectFile()
        {
            return !IsBusy;
        }

        private void SelectFile()
        {
            if (filePicker.PickFile("ABB files|*.abb|Text files|*.txt"))
                FilePath = filePicker.Path;
            else
                FilePath = FilePath ?? null;
        }

        private bool CanCancel()
        {
            return IsBusy;
        }

        private void Cancel()
        {
            sortRunner.Stop();
        }

        private bool CanRun()
        {
            return !IsBusy && FilePath != null;
        }

        private async void Run()
        {
            sortRunner.Sorter.CoreNumber = CoreNumber;
            sortRunner.Sorter.ChunkSizeInBytes = (int)ChunkSize * 1024 * 1024;

            stopwatch = new Stopwatch();
            EnableTimer(() => { Time = $"Elapsed time: {stopwatch.Elapsed.ToString("g")}"; });

            IsBusy = true;
            stopwatch.Restart();
            await sortRunner.RunOnFile(FilePath);
            stopwatch.Stop();
            IsBusy = false;

            timer.Stop();
        }

        private void EnableTimer(Action action)
        {
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(delegate { action(); });
            timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer.Start();
        }
    }
}
